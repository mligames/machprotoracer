﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEnum;

[CreateAssetMenu(fileName = "newGameManagerData", menuName = "GameManager/GameData")]
public class GameData : ScriptableObject
{
    /// <summary>
    /// The persistent stored value of the GameManager State
    /// </summary>
    public GameManager.GameState CurrentGameState;
}
