﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using GameEnum;

namespace GameEvent
{
    [Serializable] public class FloatValueChange : UnityEvent<float> { }
    [Serializable] public class EventStartRace : UnityEvent<bool> { }
    [Serializable] public class ProtoRacerCollisionEvent : UnityEvent<float> { }
}
