﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="newRaceTeamData",menuName = "ProtoRacer/Race Team Data")]
public class RaceTeamData : ScriptableObject
{
    public string RaceTeamName;
}
