﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamGarage : MonoBehaviour
{
    public RaceTeamData RaceTeamData;
    public Text TeamNameDisplay;
    // Start is called before the first frame update
    void Start()
    {
        RaceTeamData = GameManager.Instance.RaceTeamData;
        TeamNameDisplay.text = RaceTeamData.RaceTeamName;
    }
}
