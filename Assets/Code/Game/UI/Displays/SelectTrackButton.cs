﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectTrackButton : MonoBehaviour
{
    [SerializeField] private SceneData TrackScene;
    public Button TrackSelectButton;
    // Start is called before the first frame update
    void Start()
    {
        TrackSelectButton.onClick.AddListener(SelectTrack);
    }

    public void SelectTrack()
    {
        GameManager.Instance.SceneController.FadeAndLoadScene(TrackScene.SceneName);
    }
}
