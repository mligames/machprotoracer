﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapListManager : Singleton<LapListManager>
{
    public bool RaceStarted;
    public bool RaceEnded;
    public Transform LapDisplayContainer;
    public LapTimeDisplay LapTimeDisplayPrefab;
    public LapTimeDisplay CurrentLapTimeDisplay;
    // Start is called before the first frame update
    void Start()
    {
        RaceManager.Instance.OnRaceStarted.AddListener(OnRaceStart);

        CurrentLapTimeDisplay = Instantiate(LapTimeDisplayPrefab, LapDisplayContainer);
        CurrentLapTimeDisplay.SetUpDisplay(0, 0);
        CurrentLapTimeDisplay.CurrentLap = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (RaceStarted && CurrentLapTimeDisplay != null)
            CurrentLapTimeDisplay.LapTime += Time.deltaTime;
    }
    public void RegisterLap(LapTime lapTimeArg)
    {
        LapTimeDisplay temp = CurrentLapTimeDisplay;
        temp.LapNumber = lapTimeArg.Lap;
        temp.LapTime = lapTimeArg.RegisteredTime;
        temp.CurrentLap = false;

        CurrentLapTimeDisplay = Instantiate(LapTimeDisplayPrefab, LapDisplayContainer);
        CurrentLapTimeDisplay.SetUpDisplay(lapTimeArg.Lap, 0);
        CurrentLapTimeDisplay.CurrentLap = true;
    }

    public void OnRaceStart(bool raceStartStatus)
    {
        RaceStarted = raceStartStatus;
    }
}

