﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartRacePanel : MonoBehaviour
{
    public Text MessageDisplay;
    public Text TimeDisplay;
    private void Start()
    {
        TimeDisplay.text = FindObjectOfType<RaceManager>().StartingCount.ToString("F2");
        MessageDisplay.text = "Get Ready!";
    }
    private void Update()
    {
        
    }
    private void OnEnable()
    {
        TimeDisplay.text = FindObjectOfType<RaceManager>().StartingCount.ToString("F2");
        MessageDisplay.text = "Get Ready!";
    }
    private void OnDisable()
    {

    }

    public void UpdateMessageDisplay(string valueArg)
    {
        MessageDisplay.text = valueArg;
    }
    public void UpdateTimeDisplay(float valueArg)
    {
        TimeDisplay.text = valueArg.ToString("F2");
    }
}
