﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RacerStatusPanel : MonoBehaviour
{
    public ProtoRacerController ProtoRacerController;
    public Text TopSpeedDisplay;
    public Text AccelerationDisplay;
    public Text HandlingDisplay;
    public Text DriftDisplay;
    public Text DurabilityDisplay;
    public Slider DurabliltySlider;
    // Start is called before the first frame update
    void Start()
    {
        ProtoRacerController = FindObjectOfType<PlayerRacerControl>().GetComponent<ProtoRacerController>();
        ProtoRacerController.OnDurabilityChange.AddListener(UpdateDurabilityUI);

        TopSpeedDisplay.text = ProtoRacerController.ProtoRacerConfiguration.TopSpeed.ToString("F1");
        AccelerationDisplay.text = ProtoRacerController.ProtoRacerConfiguration.AccelerationForce.ToString("F1");
        HandlingDisplay.text = ProtoRacerController.ProtoRacerConfiguration.Handling.ToString("F1");
        DriftDisplay.text = ProtoRacerController.ProtoRacerConfiguration.DriftFactor.ToString("F3");
        DurabilityDisplay.text = ProtoRacerController.ProtoRacerConfiguration.Durability.ToString("F1");

        DurabliltySlider.minValue = 0;
        DurabliltySlider.maxValue = ProtoRacerController.Durability;
        DurabliltySlider.value = ProtoRacerController.Durability;
    }
    public void UpdateDurabilityUI(float amount)
    {
        DurabliltySlider.value = amount;
    } 
}
