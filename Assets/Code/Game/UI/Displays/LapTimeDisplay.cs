﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LapTimeDisplay : MonoBehaviour
{
    public bool CurrentLap;
    public int LapNumber;
    public float LapTime = 0;
    public Text LapNumberTextDisplay;
    public Text LapTimeTextDisplay;

    private void Update()
    {
        if (CurrentLap && RaceManager.Instance.RaceInProgress)
        {
            LapTime += Time.deltaTime;
            LapTimeTextDisplay.text = LapTime.ToString("F3");
        }

    }
    public void SetUpDisplay(int lapNumber, float lapTime)
    {
        LapNumber = lapNumber;
        LapTime = lapTime;
        LapNumberTextDisplay.text = lapNumber.ToString();
        LapTimeTextDisplay.text = LapTime.ToString("F3");
    }
    public void UpdateLapNumber(int lapNumber)
    {
        LapNumber = lapNumber;
        LapNumberTextDisplay.text = lapNumber.ToString();
    }

    public void UpdateLapTime(float lapTime)
    {
        LapTime = lapTime;
        LapTimeTextDisplay.text = LapTime.ToString("F3");
    }
}

