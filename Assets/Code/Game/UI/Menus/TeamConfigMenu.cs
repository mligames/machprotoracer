﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MLI.SaveSystem;
public class TeamConfigMenu : Menu
{
    [SerializeField] private GameObject menuDisplay;
    public override GameObject MenuDisplay { get => menuDisplay; set => menuDisplay = value; }

    [SerializeField] private bool menuActive;
    public override bool MenuActive { get => menuActive; set => menuActive = value; }

    public InputField TeamNameInput;
    public Button SaveTeamConfigButton;
    public Button BackButton;

    // Start is called before the first frame update
    void Awake()
    {
        TeamNameInput.onEndEdit.AddListener(TeamNameChange);
        SaveTeamConfigButton.onClick.AddListener(SaveTeamButtonPressed);
        BackButton.onClick.AddListener(BackButtonPressed);
    }
    private void OnEnable()
    {
        TeamNameInput.text = GameManager.Instance.RaceTeamData.RaceTeamName;
    }
    private void Start()
    {
        //TeamNameInput.text = Ga
    }
    public void TeamNameChange(string nameChange)
    {
        TeamNameInput.text = nameChange;
        GameManager.Instance.RaceTeamData.RaceTeamName = nameChange;
        //PlayerSaveData playerSaveData = new PlayerSaveData()
        //SaveManager.Instance.Save();
    }
    public void SaveTeamButtonPressed()
    {
        //PlayerSaveData playerSaveData = new PlayerSaveData()
        SaveManager.Instance.Save();
    }
    public void BackButtonPressed()
    {
        MenuManager.Instance.CloseMenu(this);
    }
}
