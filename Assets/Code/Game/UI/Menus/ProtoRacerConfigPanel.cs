﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProtoRacerConfigPanel : MonoBehaviour
{
    public ProtoRacerConfiguration ProtoRacerConfiguration;
    public RacerStatPanel TopSpeedStatPanel;
    public RacerStatPanel AccelerationStatPanel;
    public RacerStatPanel HandlingStatPanel;
    public RacerStatPanel DriftFactorStatPanel;
    public RacerStatPanel DurabilityStatPanel;
    public Button SaveButton;

    // Start is called before the first frame update
    void Start()
    {
        InitializeStatPanel();
        SaveButton.onClick.AddListener(OnSaveClick);
    }

    public void InitializeStatPanel()
    {
        TopSpeedStatPanel.InitializeStat(ProtoRacerConfiguration.TopSpeed);
        AccelerationStatPanel.InitializeStat(ProtoRacerConfiguration.AccelerationForce);
        HandlingStatPanel.InitializeStat(ProtoRacerConfiguration.Handling);
        DriftFactorStatPanel.InitializeStat(ProtoRacerConfiguration.DriftFactor);
        DurabilityStatPanel.InitializeStat(ProtoRacerConfiguration.Durability);
    }
    public void OnSaveClick()
    {
        ProtoRacerConfiguration.TopSpeed = TopSpeedStatPanel.Value;
        ProtoRacerConfiguration.AccelerationForce = AccelerationStatPanel.Value;
        ProtoRacerConfiguration.Handling = HandlingStatPanel.Value;
        ProtoRacerConfiguration.DriftFactor = DriftFactorStatPanel.Value;
        ProtoRacerConfiguration.Durability = DurabilityStatPanel.Value;
    }
}
