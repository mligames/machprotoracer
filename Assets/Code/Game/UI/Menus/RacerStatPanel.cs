﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RacerStatPanel : MonoBehaviour
{
    public string StatName;
    public float MinValue;
    public float MaxValue;
    public float Value;
    public Text StatLabel;
    public Slider StatSlider;
    public InputField StatInputField;
    // Start is called before the first frame update
    void Start()
    {
        StatLabel.text = StatName;
        StatSlider.minValue = MinValue;
        StatSlider.maxValue = MaxValue;

        StatSlider.onValueChanged.AddListener(OnStatSliderChange);
        StatInputField.onValueChanged.AddListener(OnStatInputFieldChange);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetValue(float valueArg)
    {
        Value = valueArg;
        StatInputField.text = valueArg.ToString();
        StatSlider.value = valueArg;
    }
    public void InitializeStat(float valueArg)
    {
        Value = valueArg;
        StatSlider.value = valueArg;
        StatInputField.text = valueArg.ToString();
    }
    public void OnStatSliderChange(float valueArg)
    {
        SetValue(valueArg);
    }
    public void OnStatInputFieldChange(string valueArg)
    {
        SetValue(float.Parse(valueArg));       
    }
}
