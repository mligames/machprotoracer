﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VehicleConfigMenu : MonoBehaviour
{
    public ProtoRacerConfiguration ProtoRacerConfiguration;

    [Header("Vehicle Settings")]
    public Slider DriveForceSlider;
    public Slider HandlingSlider;
    public Slider BrakingVelocityFactorSlider;
    public Slider DriftFactorSlider;
    public Slider DurabilitySlider;
    //[Header("Hover Settings")]
    //public Slider HoverForceSlider;
    //public Slider HoverHeightSlider;
    //public Slider MaxGroundDistanceSlider;
    //[Header("Physics Settings")]
    public Slider TerminalVelocitySlider;
    //public Slider HoverGravitySlider;
    //public Slider FallGravitySlider;
    private void OnEnable()
    {
        LoadVehicleConfig();
    }
    public void LoadVehicleConfig()
    {
        DriveForceSlider.minValue = ProtoRacerMinMaxStat.AccelerationForceMin;
        DriveForceSlider.maxValue = ProtoRacerMinMaxStat.AccelerationForceMax;
        DriveForceSlider.value = ProtoRacerConfiguration.AccelerationForce;

        HandlingSlider.minValue = ProtoRacerMinMaxStat.HandlingMin;
        HandlingSlider.maxValue = ProtoRacerMinMaxStat.HandlingMax;
        HandlingSlider.value = ProtoRacerConfiguration.Handling;

        BrakingVelocityFactorSlider.minValue = 0;
        BrakingVelocityFactorSlider.maxValue = 1;
        BrakingVelocityFactorSlider.value = ProtoRacerConfiguration.BrakingVelocityFactor;

        DriftFactorSlider.minValue = ProtoRacerMinMaxStat.DriftFactorMin;
        DriftFactorSlider.maxValue = ProtoRacerMinMaxStat.DriftFactorMax;
        DriftFactorSlider.value = ProtoRacerConfiguration.DriftFactor;

        DurabilitySlider.minValue = ProtoRacerMinMaxStat.DurabilityMin;
        DurabilitySlider.maxValue = ProtoRacerMinMaxStat.DurabilityMax;
        DurabilitySlider.value = ProtoRacerConfiguration.Durability;

        TerminalVelocitySlider.minValue = ProtoRacerMinMaxStat.TopSpeedMin;
        TerminalVelocitySlider.maxValue = ProtoRacerMinMaxStat.TopSpeedMax;
        TerminalVelocitySlider.value = ProtoRacerConfiguration.TopSpeed;

        //HoverForceSlider.value = ProtoRacerConfiguration.HoverForce;
        //HoverHeightSlider.value = ProtoRacerConfiguration.HoverHeight;
        //MaxGroundDistanceSlider.value = ProtoRacerConfiguration.MaxGroundDistance;
        //HoverGravitySlider.value = ProtoRacerConfiguration.HoverGravity;
        //FallGravitySlider.value = ProtoRacerConfiguration.FallGravity;
    }
    public void OnSaveVehicleConfigPressed()
    {
        ProtoRacerConfiguration.AccelerationForce = DriveForceSlider.value;
        ProtoRacerConfiguration.Handling = HandlingSlider.value;
        ProtoRacerConfiguration.BrakingVelocityFactor = BrakingVelocityFactorSlider.value;
        ProtoRacerConfiguration.DriftFactor = DriftFactorSlider.value;
        ProtoRacerConfiguration.Durability = DurabilitySlider.value;
        //ProtoRacerConfiguration.HoverForce = HoverForceSlider.value;
        //ProtoRacerConfiguration.HoverHeight = HoverHeightSlider.value;
        //ProtoRacerConfiguration.MaxGroundDistance = MaxGroundDistanceSlider.value;
        ProtoRacerConfiguration.TopSpeed = TerminalVelocitySlider.value;
        //ProtoRacerConfiguration.HoverGravity = HoverGravitySlider.value;
        //ProtoRacerConfiguration.FallGravity = FallGravitySlider.value;
    }

}
