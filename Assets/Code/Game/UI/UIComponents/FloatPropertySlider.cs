﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatPropertySlider : MonoBehaviour
{
    public Text PropertyNameText;
    public Text PropertyValueText;
    public string PropertyName;
    public int FloatFormat = 1;
    private void Start()
    {
        PropertyNameText.text = PropertyName;
    }
    public void UpdatePropertyValue(float valueArg)
    {
        PropertyValueText.text = valueArg.ToString("F"+FloatFormat);
    }
}
