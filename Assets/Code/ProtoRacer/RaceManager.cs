﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvent;
public class RaceManager : Singleton<RaceManager>
{
    public ProtoRacerController[] CurrentRacers;
    public float StartingCount;
    public float CountdownTimer;
    public float DisplayLingerTime;
    public bool PreRaceCheckComplete;
    public bool RaceInProgress;
    public StartRacePanel StartRacePanel;
    public EventStartRace OnRaceStarted;
    // Start is called before the first frame update
    void Start()
    {
        PreRaceCheckComplete = false;
        RaceInProgress = false;
        StartCoroutine(PreRaceSetUpRoutine());
        StartRacePanel.MessageDisplay.text = "Get Ready!";
        StartRacePanel.UpdateTimeDisplay(CountdownTimer);
    }
    // Update is called once per frame
    void Update()
    {
        if (PreRaceCheckComplete && !RaceInProgress)
        {
            CountdownTimer = Mathf.Clamp(CountdownTimer -= Time.deltaTime, 0f, StartingCount);
            StartRacePanel.UpdateTimeDisplay(CountdownTimer);
            if (CountdownTimer <= 0)
            {
                StartCoroutine(StartRaceRoutine());
                StartRacePanel.gameObject.SetActive(false);
                RaceInProgress = true;
                OnRaceStarted.Invoke(RaceInProgress);

            }
        }
    }
    IEnumerator PreRaceSetUpRoutine()
    {
        CurrentRacers = FindObjectsOfType<ProtoRacerController>();

        foreach (var racer in CurrentRacers)
        {
            racer.RacerLocked = true;
        }

        CountdownTimer = StartingCount;

        yield return null;

        PreRaceCheckComplete = true;
    }

    IEnumerator StartRaceRoutine()
    {
        CurrentRacers = FindObjectsOfType<ProtoRacerController>();
        foreach (var racer in CurrentRacers)
        {
            racer.RacerLocked = false;
        }
        yield return null;
    }
}
