﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapTracker : MonoBehaviour
{
    public bool RaceStarted;
    public bool CrossedStartingLine;
    public int CurrentLap;
    public float CurrentLapTime = 0f;
    //public bool LapRequirementsMet;
    public List<LapTime> LapTimesList;
    private void Start()
    {
        LapTimesList = new List<LapTime>();
        AddLapTime(CurrentLap, 0);
        RaceManager.Instance.OnRaceStarted.AddListener(OnRaceStart);
    }
    private void Update()
    {
        if(CrossedStartingLine)
            CurrentLapTime += Time.deltaTime;
    }
    public void OnRaceStart(bool raceStartStatus)
    {
        RaceStarted = true;
    }
    public LapTime CurrentLapInfo()
    {
        return LapTimesList[LapTimesList.Count - 1];
    }
    public void MarkLapTime()
    {
        CurrentLap++;
        AddLapTime(CurrentLap, CurrentLapTime);
    }
    public void AddLapTime(int lapNumber, float lapTime)
    {
        LapTimesList.Add(new LapTime(lapNumber, lapTime));
    }
}
