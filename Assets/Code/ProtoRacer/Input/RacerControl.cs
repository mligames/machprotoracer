﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RacerControl : MonoBehaviour
{
    public bool IsPlayer;
    public abstract bool Accelerate { get; set; }
    public abstract bool Braking { get; set; }
    public abstract float SteerDirection { get; set; }
}
