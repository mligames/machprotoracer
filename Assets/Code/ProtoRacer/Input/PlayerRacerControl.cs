﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRacerControl : RacerControl
{
    public KeyCodeVariable AccelerateKey;
    public KeyCodeVariable BrakeKey;
    public KeyCodeVariable SteerLeft;
    public KeyCodeVariable SteerRight;

    [SerializeField] private float steeringDirection;
    public override float SteerDirection { get => steeringDirection; set => steeringDirection = value; }

    [SerializeField] private bool isAccelerating;
    public override bool Accelerate { get => isAccelerating; set => isAccelerating = value; }

    [SerializeField] private bool isBraking;
    public override bool Braking { get => isBraking; set => isBraking = value; }

    private void Update()
    {
        SteerDirection = 0;

        if (AccelerateKey.KeyPressBoolValue())
        {
            Accelerate = true;
        }
        else
        {
            Accelerate = false;
        }

        if (BrakeKey.KeyPressBoolValue())
        {
            Braking = true;
        }
        else
        {
            Braking = false;
        }
        if (SteerLeft.KeyPressBoolValue())
        {
            SteerDirection -= SteerLeft.KeyPressIntValue();
        }
        if (SteerRight.KeyPressBoolValue())
        {
            SteerDirection += SteerRight.KeyPressIntValue();
        }
    }
}
