﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct LapTime
{
    public int Lap;
    public float RegisteredTime;
    public LapTime(int lap, float registeredTime)
    {
        Lap = lap;
        RegisteredTime = registeredTime;
    }
}
