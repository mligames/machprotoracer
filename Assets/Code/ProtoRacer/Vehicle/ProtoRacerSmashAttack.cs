﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtoRacerSmashAttack : MonoBehaviour
{
    public Collider ProtoRacerCollider;
    public BoxCollider attackCollider;
    public float AttackDuration;
    // Start is called before the first frame update
    void Start()
    {
        Physics.IgnoreCollision(attackCollider, ProtoRacerCollider);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        ProtoRacerController protoRacerController = other.transform.root.GetComponent<ProtoRacerController>();
        if (protoRacerController != null)
        {
            Debug.Log("Hit " + protoRacerController.name);
        }
    }
    public void PerformAttack()
    {
        attackCollider.enabled = true;
        StopCoroutine(AttackSequence());
        StartCoroutine(AttackSequence());
    }
    IEnumerator AttackSequence()
    {
        attackCollider.enabled = true;
        yield return new WaitForSeconds(AttackDuration);
        attackCollider.enabled = false;
    }
}
