﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="newProtRacerConfig", menuName = "ProtoRacer/Racer Vehicle Config")]
public class ProtoRacerConfiguration : ScriptableObject
{
    //[Header("Driver")]

    [Header("Vehicle Settings")]
    public float TopSpeed;
    public float AccelerationForce;
    public float Handling;
    public float DriftFactor;
    public float Durability;
    [Range(0, 1)]
    public float BrakingVelocityFactor;

    public void InitializeVariables(ProtoRacerController protoRacerController)
    {
        protoRacerController.TopSpeed = TopSpeed;
        protoRacerController.AccelerationForce = AccelerationForce;
        protoRacerController.Handling = Handling;
        protoRacerController.DriftFactor = DriftFactor;
        protoRacerController.Durability = Durability;
        protoRacerController.BrakingVelFactor = BrakingVelocityFactor;
    }
}
