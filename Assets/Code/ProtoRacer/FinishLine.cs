﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinishLine : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        LapTracker lapTracker = other.GetComponentInParent<LapTracker>();

        if(lapTracker != null)
        {
            if (lapTracker.CrossedStartingLine)
            {
                lapTracker.MarkLapTime();
                if (lapTracker.gameObject.tag == "Player")
                {
                    LapListManager.Instance.RegisterLap(new LapTime(lapTracker.CurrentLap, lapTracker.CurrentLapTime));
                }
            }
            else
            {
                lapTracker.CrossedStartingLine = true;
            }
        }
    }
}
