﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StopWatch : MonoBehaviour
{
    public float timer;
    // Start is called before the first frame update
    void Start()
    {
        timer = 0f;
    }

    public float MarkTime()
    {
        return timer;
    }
    public void ResetTimer()
    {
        timer = 0f;
    }

}
