﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : Menu
{
    [SerializeField] private GameObject menuDisplay;
    public override GameObject MenuDisplay { get => menuDisplay; set => menuDisplay = value; }

    [SerializeField] private bool menuActive;
    public override bool MenuActive { get => menuActive; set => menuActive = value; }
    //public Button OpenTeamConfigButton;
    public Button BackButton;

    // Start is called before the first frame update
    void Awake()
    {
        //OpenTeamConfigButton.onClick.AddListener(OpenTeamButtonPressed);
        BackButton.onClick.AddListener(BackButtonPressed);
    }
    public void OpenTeamButtonPressed()
    {
        //MenuManager.Instance.OpenMenu(MenuManager.Instance.TeamConfigMenu);
    }
    public void BackButtonPressed()
    {
        MenuManager.Instance.CloseMenu(this);
    }
    
}
