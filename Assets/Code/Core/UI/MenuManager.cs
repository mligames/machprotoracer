﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : Singleton<MenuManager>
{
    public StartMenu StartMenu;
    public PauseMenu PauseMenu;
    public OptionsMenu OptionsMenu;
    //public TeamConfigMenu TeamConfigMenu;
    public Stack<Menu> MenuStack;
    protected override void Awake()
    {
        base.Awake();
        MenuStack = new Stack<Menu>();
    }

    // Update is called once per frame
    void Update()
    {       
        if (Input.GetKeyDown(KeyCode.Escape) && GameManager.Instance.SceneController.CurrentScene != GameManager.Instance.SceneController.Scenes[0])
        {
            if (!PauseMenu.MenuActive)
            {
                OpenMenu(PauseMenu);
                GameManager.Instance.TogglePause();
            }

        }
        else if (Input.GetKeyDown(KeyCode.Escape) && GameManager.Instance.SceneController.CurrentScene == GameManager.Instance.SceneController.Scenes[0])
        {
            GameManager.Instance.QuitGame();
        }
    }

    public void OpenMenu(Menu menuArg)
    {
        //if(MenuStack.Count > 0)
        //{
        //    foreach (var menu in MenuStack)
        //    {
        //        CloseMenu(menu);              
        //    }
        //}
        menuArg.transform.SetAsLastSibling();
        menuArg.Open();
        MenuStack.Push(menuArg);
        Debug.Log(menuArg.name +" Openned MenuStack.Count = " + MenuStack.Count);
        //StartCoroutine(ChangeMenuSequence(menu));
    }
    public void CloseMenu(Menu menuArg)
    {
        menuArg.Close();
        //Menu popMenu = MenuStack.Pop();
        //Debug.Log(popMenu.name + "Popped");
        if (MenuStack.Count > 0)
        {
            MenuStack.Pop();
        }

        if (MenuStack.Count > 0)
        {
            MenuStack.Peek().Open();
        }
        Debug.Log(menuArg.name + " Closed MenuStack.Count = " + MenuStack.Count);
        //StartCoroutine(ChangeMenuSequence(menu));
    }
    //public void CloseTopMenu(Menu menu)
    //{
    //    menu.Close();
    //    //StartCoroutine(ChangeMenuSequence(menu));
    //}
    //public void ChangeMenu(Menu menu)
    //{
    //    StartCoroutine(ChangeMenuSequence(menu));
    //}
    //private IEnumerator ChangeMenuSequence(Menu menu)
    //{
    //    if (CurrentActiveMenu != null)
    //    {
    //        CurrentActiveMenu.Close();
    //    }
    //    yield return null;
    //    menu.Open();
    //}
}
