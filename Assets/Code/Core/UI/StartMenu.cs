﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartMenu : Menu
{
    [SerializeField] private GameObject menuDisplay;
    public override GameObject MenuDisplay { get => menuDisplay; set => menuDisplay = value; }

    [SerializeField] private bool menuActive;
    public override bool MenuActive { get => menuActive; set => menuActive = value; }

    public Button StartGameButton;
    public Button OptionMenuButton;
    //public Button QuitGameButton; // For Non WebGL Builds
    // Start is called before the first frame update
    void Awake()
    {
        StartGameButton.onClick.AddListener(StartGameButtonPressed);
        OptionMenuButton.onClick.AddListener(OptionMenuButtonPressed);
        //QuitGameButton.onClick.AddListener(QuitGameButtonPressed);
    }

    public void StartGameButtonPressed()
    {
        GameManager.Instance.StartGame();
    }

    public void OptionMenuButtonPressed()
    {
        MenuManager.Instance.OpenMenu(MenuManager.Instance.OptionsMenu);
    }
    public void QuitGameButtonPressed()
    {
        GameManager.Instance.QuitGame();
    }

}
