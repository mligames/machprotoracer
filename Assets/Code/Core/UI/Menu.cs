﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Menu : MonoBehaviour
{
    public abstract GameObject MenuDisplay { get; set; }
    public abstract bool MenuActive { get; set; }
    public virtual void Open()
    {
        MenuActive = true;
        MenuDisplay.SetActive(true);
    }

    public virtual void Close()
    {
        MenuActive = false;
        MenuDisplay.SetActive(false);
    }
}
