﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : Menu
{
    [SerializeField] private GameObject menuDisplay;
    public override GameObject MenuDisplay { get => menuDisplay; set => menuDisplay = value; }

    [SerializeField] private bool menuActive;
    public override bool MenuActive { get => menuActive; set => menuActive = value; }


    public Button ResumeGameButton;
    public Button OptionMenuButton;
    public Button QuitToTeamGarageButton;
    public Button QuitToTitleButton;

    // Start is called before the first frame update
    void Awake()
    {
        ResumeGameButton.onClick.AddListener(ResumeGameButtonPressed);
        OptionMenuButton.onClick.AddListener(OptionMenuButtonPressed);
        QuitToTeamGarageButton.onClick.AddListener(QuitToTeamGarageButtonPressed);
        QuitToTitleButton.onClick.AddListener(QuitToTitleButtonPressed);
    }

    public void ResumeGameButtonPressed()
    {
        MenuManager.Instance.CloseMenu(this);
        GameManager.Instance.TogglePause();
    }
    public void OptionMenuButtonPressed()
    {
        MenuManager.Instance.OpenMenu(MenuManager.Instance.OptionsMenu);
    }
    public void QuitToTeamGarageButtonPressed()
    {
        GameManager.Instance.TogglePause();
        GameManager.Instance.SceneController.FadeAndLoadScene("TeamGarage");
    }
    public void QuitToTitleButtonPressed()
    {
        GameManager.Instance.TogglePause();
        GameManager.Instance.QuitToTitle();
    }

}
