﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
namespace MLI.SaveSystem
{
    public class SaveManager : Singleton<SaveManager>
    {
        
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void Save()
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            Debug.Log(Application.persistentDataPath);
            if (!File.Exists(Application.persistentDataPath + "/PlayerSaveData.dat"))
                File.Create(Application.persistentDataPath + "/PlayerSaveData.dat");
            FileStream file = File.Open(Application.persistentDataPath + "/PlayerSaveData.dat", FileMode.Open);
            
            PlayerSaveData playerSaveData = new PlayerSaveData(GameManager.Instance.RaceTeamData.RaceTeamName);
            binaryFormatter.Serialize(file, playerSaveData);
            file.Close();

        }
    }
    [Serializable]
    class PlayerSaveData
    {
        public string RacingTeamName { get; set; }
        public PlayerSaveData(string racingTeamName)
        {
            RacingTeamName = racingTeamName;
        }
    }
}