﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newKeyMap", menuName = "KeyCode")]
public class KeyCodeVariable : ScriptableObject
{
#if UNITY_EDITOR
    [Multiline]
    public string DeveloperDescription = "";
#endif
    public KeyCode KeyAssignment;

    public bool KeyDownBoolValue()
    {
        if (Input.GetKeyDown(KeyAssignment))
            return true;
        else
            return false;
    }
    public bool KeyUpBoolValue()
    {
        if (Input.GetKeyUp(KeyAssignment))
            return true;
        else
            return false;
    }
    public bool KeyPressBoolValue()
    {
        if (Input.GetKey(KeyAssignment))
            return true;
        else
            return false;
    }
    public int KeyDownIntValue()
    {
        if (Input.GetKeyDown(KeyAssignment))
            return 1;
        else
            return 0;
    }
    public int KeyUpIntValue()
    {
        if (Input.GetKeyUp(KeyAssignment))
            return 1;
        else
            return 0;
    }
    public int KeyPressIntValue()
    {
        if (Input.GetKey(KeyAssignment))
            return 1;
        else
            return 0;
    }
}
