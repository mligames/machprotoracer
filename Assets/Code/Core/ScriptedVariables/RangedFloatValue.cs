﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newRangedFloatVar", menuName = "Variables/Ranged Float")]
public class RangedFloatValue : ScriptableObject
{
#if UNITY_EDITOR
    [Multiline]
    public string DeveloperDescription = "";
#endif
    public float MinValue;
    public float MaxValue;
    public float Value;

    public void SetValue(float value)
    {
        Value = value;
    }

    public void SetValue(FloatVariable value)
    {
        Value = value.Value;
    }

    public void ApplyChange(float amount)
    {
        Value += amount;
    }

    public void ApplyChange(FloatVariable amount)
    {
        Value += amount.Value;
    }
#if UNITY_EDITOR
    private void OnValidate()
    {
        Value = Mathf.Clamp(Value, MinValue, MaxValue);
    }
#endif
}
