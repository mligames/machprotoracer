﻿// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// Modified: William O'Toole
// ----------------------------------------------------------------------------
using UnityEngine;
[CreateAssetMenu(fileName = "newFloatVar", menuName = "Variables/Float")]
public class FloatVariable : ScriptableObject
{    
#if UNITY_EDITOR
    [Multiline]
    public string DeveloperDescription = "";
#endif
    public float MinValue;
    public float MaxValue;
    public float Value;
    public bool RangedValue;

    public void SetValue(float value)
    {
        Value = value;
        if (RangedValue)
            Value = Mathf.Clamp(Value, MinValue, MaxValue);
    }

    public void SetValue(FloatVariable value)
    {
        Value = value.Value;
        if (RangedValue)
            Value = Mathf.Clamp(Value, MinValue, MaxValue);
    }

    public void ApplyChange(float amount)
    {
        Value += amount;
        if (RangedValue)
            Value = Mathf.Clamp(Value, MinValue, MaxValue);
    }

    public void ApplyChange(FloatVariable amount)
    {
        Value += amount.Value;
        if (RangedValue)
            Value = Mathf.Clamp(Value, MinValue, MaxValue);
    }
#if UNITY_EDITOR
    private void OnValidate()
    {
        if (RangedValue)
            Value = Mathf.Clamp(Value, MinValue, MaxValue);
    }
#endif
}
