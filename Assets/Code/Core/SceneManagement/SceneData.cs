﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newSceneAsset", menuName = "Scene Managment/SceneAsset")]
public class SceneData : ScriptableObject
{
    [SerializeField] private string sceneName;
    public string SceneName { get => sceneName; private set => sceneName = value; }
    [TextArea]
    public string sceneDescription;
#if UNITY_EDITOR
    public UnityEditor.SceneAsset Scene;
    private void OnValidate()
    {
        //collect the scene name
        if (Scene != null)
            SceneName = Scene.name;
        else
            SceneName = "";
    }
#endif
}
